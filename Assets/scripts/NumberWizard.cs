﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
//using System.Collections;

public class NumberWizard : MonoBehaviour {

	public Text guessText;
	public Text countText;
	//public int guessLimit = 16;
	int guessLimit = 13;
	int min, max, guess, count, cap;

	
	void Start () {	
		Startgame ();
	}
	
	void Startgame (){
	    max = 1000;
	    min = 1;
		count = 0;
		
		//random selector method does not allow max to be selected
		//this enables the max range to be available on guess #1
		max = max + 1;
		cap = max;
		NextGuess ();	
	}

	// Update is called once per frame
	//
	void Update () {
		if (count == guessLimit +1) {
			SceneManager.LoadScene ("Win");
		}
	}

	public void GuessHigher () {
	
		//clip min to <= max at threshold values
		//to prevent user spam clicks
		if (min >= max) { 
			min = max; 
		}
		else {
		//min needs to be higher than the floor
		//else previous "too low" guess could be chosen again
			min = guess + 1;
		}
        NextGuess ();
	}

    public void GuessLower ()
    {
        max = guess;
        NextGuess ();
    }

	public void GuessCorrect () {
	    SceneManager.LoadScene ("Lose");
	}

	public void NextGuess () {
	count++;
	guess = Random.Range (min, max);
	
		// prevents cap being selected erroneously
		//
		if (guess == cap) {
			guess = cap -1;
		}
		
		// if statement catches the game from printing the next guess AFTER correct has being chosen
		//
		if (count != guessLimit + 1) {
			guessText.text = "Is your number " + guess + " ?";
			countText.text = (guessLimit - count) + " guesses left and you win..";
			//debug printing to display the available range +guess
			//
			print("[ " + min + " <= " + guess + " < " + max + " ]" + " @" + count);
		}
	
	}
}
