﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

	public void LoadLevel (string name) {
		Debug.Log("Level Load requested for: "+name);
		SceneManager.LoadScene (name);
	}
	public void QuitRequest () {
		Debug.Log ("Quit Request initiated");
		Application.Quit ();
	}
}
